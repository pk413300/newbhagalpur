const Cityprofile = () => {

  return (
    <>

      <section className="main-body" style={{ padding: '30px 0px' }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670 }}>CITY PROFILE</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Bhagalpur, the 3rd largest city in Bihar state is a
            divisional town with historical importance situated on the southern bank of the Ganga river. The
            Gangetic plains surrounding the city are very fertile. The city is a district headquarters serving
            multiple functions of an administrative, trade and commerce, service and distribution centre.
            Vikramshila Dolphin Sanctuary established near the town is the only sanctuary in Asia for conservation
            of Gangetic Dolphins. World’s second largest rescue and rehabilitation area for Garuda is located in
            Bhagalpur. The city is well connected through rail and road networks. NH-80 runs through the city and
            connects with Patna. SH-19 and 25 are the main arterial roads. Bhagalpur has been associated with silk
            industry for hundreds of years and is also known as Silk City and ranks 2nd after Karnataka in silk
            fabric production and exports. The city is also famous for its Manjusha paintings, a unique art found
            only in Bihar.
          </p>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Bhagalpur Nagar Nigam covering 30.17 sq.km. area is
            divided into 51 wards. As of 2011 census, Bhagalpur town has a population of more than 4 lakhs with
            average literacy rate of 81%. The competitive advantage of Bhagalpur lies in its tradition as a silk
            weaving centre, the potential for riverfront development, the potential to develop it as a logistics hub
            to promote trade and commerce and development of open spaces. The city development plan prepared for
            Bhagalpur envisions its development as premier silk production centre and hub for agriculture produce
            with higher order infrastructure facilities. Maximum Summer Temp. &amp; Minimum Winter temp. are 44°C and
            10°C.
          </p>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Bhagalpur is growing and emerging as trade, business, and
            education centre in last decade and witnessing increase in migration from immediate hinterland and
            different part of the state of Bihar. It resulted in to rapid urbanization in neighboring areas of
            Bhagalpur Municipal Area become outgrowths of Bhagalpur City. In the absence of planning interventions,
            rapid growth led to haphazard development which resulted into deterioration of open space and green
            area, uncontrolled and unregulated construction activities, formation of slum and unregulated
            construction within core city. Mixed land use is prevalent along major roads. Road frontage are
            dominated by commercial activities and retail shops. The city has about 86,000 households with average
            density of 13,263 persons per sq.km. There are about 165 slums spread within city with population of
            about 9,000 persons.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>Social Infrastructure</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Bhagalpur, being a district headquarters has good quality social infrastructure facilities including higher level education and health facilities. However, recreational and community facilities are insufficient. Jawaharlal Nehru Medical College &amp; Hospital is the largest hospital in the city as well as district with more than 700 beds and modern equipment’s. Recreational facilities include Jayaprakash Udyan (city level park), Lajpat Park, Forest and Railway Park, Town Hall and Vivah Bhawan.  The city has more than 160 slums housing about 30% of city population. About more than 40% of households are below poverty line. Basic facilities are practically absent in slums. Situation is aggravated by poor coverage of poverty and slum upgradation schemes. 90% of road length in slums is kutchha without any drainage network. Hand pump is the primary source of water and one pump serves more than 70 households. 90% of slums are of kutcha or semi-kutcha in nature with inadequate access to health facilities. Annual tourist inflow of tourist is estimated at 6% with highest inflow recorded during July-August at time of regional festivals. Tourist infrastructure is inadequate, heritage structures are poorly maintained.</p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>Institutions</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>The city is a hub of educational activities for surrounding towns and villages. There are more than 400 government schools comprising more than 300 primary school, more than 70 middle schools and more than 20 high schools, including about 6 degree colleges, 1 engineering college, 1 medical college, agriculture university, ITI and polytechnic for higher and technical education facilities. There are both, private and government schools. Some famous institutions are Tilakmanjhi Bhagalpur university, Bhagalpur college of engineering, Bihar agricultural university, Indian Institute of Information Technology, St. Joseph’s school, Delhi Public School, TNB college etc.</p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>Civic Amenities</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Water supply in Bhagalpur is based on surface water and ground water. Town receives about 17 MLD of piped water from river Ganga. Tube wells account for 18 MLD of supply and hand pumps account for 2.25 MLD.  It is estimated that 38 MLD water is supplied covering 19% of the town. Current installed storage capacity is 67% of supply, there are 7 over head tanks. Ground water is high in nitrate and arsenic. Per capita water supply is estimated at 33 lpcd and supply is for 7 hours a day. The city is implementing an integrated water supply scheme from state funds. Bhagalpur has minimal sewerage system. The estimated generation is 44 MLD waste water. Town has one treatment plant with installed capacity of 11 MLD. Only 4 MLD of waste water is collected and treated (primary) with remaining waste water being discharged into river Ganga with no treatment. 75% households have individual toilets with septic tanks and soak pits. Town has 26 community toilets but all are poorly maintained. Only 20% of town has sewage network services. The total drainage network in Bhagalpur municipal area covers 162 km. of which 112 km. drains are pucca open and 50 km. are kutcha open drains. Drains are normally seen on one side of the road. Natural drainage is from south to north. Existing drainage system is highly chocked by indiscriminate garbage dumping and silting. All discharge from drains is disposed directly into river Ganga. Storm water drains carry waste water from households. Bhagalpur generates about 225 tonnes of waste per day of which 87% is collected. Door to door collection and transportation system was introduced last year with road sweeping and drain cleaning through a private operator in 10 wards of the town. Nagar Nigam has limited equipment’s for solid waste management. There is no treatment of garbage prior to disposal. At present, garbage is dumped on available open spaces. There are more than 2000 street light poles in the city.</p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>Economy</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Workforce participation in the town is about 26%. Main workers account for 84%. Bhagalpur’s economic base depends on silk production, trade and commerce. Annual silk production is estimated 2 million meters per year of tussar silk. The town is district level wholesale market for agriculture produce. Barari industrial area is the largest industrial belt in the district covering 51 acres and Bahadurpur is bout 15 acres both dedicated to cottage industries. kahalgaon industrial area also being equally important. Government of India has established a handloom park and food park has been established by private entrepreneurs. The town has a strong presence of informal sector- about 6800 units contributing to 30-40% of city economy. Bhagalpur has significant potential to develop as a ‘silk city’ of India. About 50% of silk produced is exported.</p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>Transport &amp; Connectivity</h4>
          <p style={{ margin: '0px 0px 10px', color: '#3f3f3f' }}>Bhagalpur town has 175 km of roads with a density of 5.8 (ratio of total road length to city area). The city is well connected by roadways and railways with other cities, towns and villages of the state. Bhagalpur Railway Station is the busiest railway station and is connected with many states of the country. Main roads in town include NH-80, SH 19 &amp; 25. The road network is largely undivided with no footpaths, high levels of encroachments, free and haphazard parking contributing to traffic jams. Intra city transport is mainly by way of cycles and auto rickshaws, while intercity transport is catered by state transport buses. 6 seater diesel rickshaw is the major mode of intermediate public transport and the prime cause of traffic jams and pollution. These diesel rickshaws are emitting smoke and polluting the city air. Besides that, cycle rickshaws are also operated on some of the roads. The city growth profile is of linear in nature from east to west and south but the road network system is deficient in terms of adequate widths and traffic management aspects.  Some roads have asphalt whereas some roads have cemented concrete road surface. Road maintenance activities are irregular in nature</p>
        </div>
      </section>
    </>
  )
}

export default Cityprofile;