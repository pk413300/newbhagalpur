import React, { useState } from "react";
import { IntlProvider } from "react-intl";
import Hindi from "./lang/hi.json";

import English from "./lang/en.json";

export const Context = React.createContext();

const local = navigator.language;

let lang;
if (local === "en-US") {
  lang = English;
} else {
  lang = Hindi;
}

const Wrapper = (props) => {
  const [locale, setLocale] = useState(local);

  const [messages, setMessages] = useState(lang);

  function selectLanguage(e) {
    const newLocale = e.target.value;
    setLocale(newLocale);
    if (newLocale === "en") {
      setMessages(English);
    } else {
      setMessages(Hindi);
    }
  }

  return (
    <Context.Provider value={{ locale, selectLanguage }}>
      <IntlProvider messages={messages} locale={locale}>
        {props.children}
      </IntlProvider>
    </Context.Provider>
  );
};

export default Wrapper;
