import React from "react";
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
// const Nav = React.lazy(() => import("./Nav/Nav"));
// const Home = React.lazy(() => import("./Home/Home"));
// const Footer = React.lazy(() => import("./Footer/Footer"));
// const Aboutcity = React.lazy(() => import("./Aboutcity/Aboutcity"));

import Nav from "./Nav/Nav";
import Home from "./Home/Home";
import Footer from "./Footer/Footer";
import Aboutcity from "./Aboutcity/Aboutcity";
import CityProfile from "./Cityprofile/Cityprofile";
import BSCL from "./BSCL/BSCL";
import UnderDev from "./UnderDev/UnderDev";
import ContactUs from "./ContactUs/ContactUs";

const App = () => {
  return (
    <Router>
      <Nav />
      <Routes>
        <Route path="/contact" element={<ContactUs />}></Route>
        <Route path="/underconstruction" element={<UnderDev />}></Route>
        <Route path="/team" element={<BSCL />}></Route>
        <Route path="/aboutcity" element={<Aboutcity />}></Route>
        <Route path="/cityprofile" element={<CityProfile />}></Route>
        <Route path="/" element={<Home />}></Route>
      </Routes>
      <Footer />
    </Router>
  );
};

export default App;
