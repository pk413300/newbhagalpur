import "./Home.css"
const Home = () => {

  return (
    <>
      <main className="site-main">
        <section className="hero_area" >
          <div className="hero_content">
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="important">
          <div className="container">
            <div className="row">
              <div className="header-bottom1" style={{ marginTop: '-5em' }}>
                <div className="col-sm-4 box1">
                  <span className="heading2"><b>Message Label</b></span>
                </div>
                <div className="col-sm-8 box2">
                  <span className="heading2">
                    <marquee scrollamount="6"><b>Development of sewerage system &amp; under ground main storm water drainage
                      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Renovation of town hall &nbsp; &nbsp; &nbsp;
                      &nbsp; &nbsp; Construction of ICCC building in MRDA(D) campus</b></marquee>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <h2 className="section-title" style={{ paddingBottom: '20px' }}><b>IMPORTANTS LINKS</b></h2>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media Know_your_polling">
                  <div className="media-left media-middle book">
                    <img src="img/icon-03-1.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">PROJECT<br /> DETAILS</h4>
                  </div>
                </div>
              </div>

              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media Know_your_polling1">
                  <div className="media-left media-middle book">
                    <img src="img/img-04.png" style={{ width: '50px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">ONGOING <br />WORK</h4>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media Know_your_polling1">
                  <div className="media-left media-middle book">
                    <img src="img/img-01-1.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">REMAINING <br />WORK</h4>
                  </div>
                </div>
              </div>

              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media Know_your_polling1">
                  <div className="media-left media-middle book">
                    <img src="img/img1.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">PROJECT <br />STATUS</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <h2 className="section-title" style={{ paddingBottom: '20px', paddingTop: '20px' }}><b>RECENT PROJECTS </b>
              </h2>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <img src="bhgImg//01_11_2021-tample_of_bhagalpur__22169741_750x500_617fa20861d8b (1).jpg" style={{ width: '556px', height: '140px' }} className="img-responsive  left-block-shape1" alt=""/>
                <span className="heading2 itemcap">
                  <marquee scrollamount="4" ><b>Development of sewerage system &amp; under ground main storm water drainage.</b>
                  </marquee>
                </span>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <img src="bhgImg/3bddfb3649bae36cc801fe8b9280bedd (1).jpg" style={{ width: '556px', height: '140px' }} className="img-responsive  left-block-shape1" alt="" />
                <span className="heading2 itemcap">
                  <marquee scrollamount="4"><b>Beautification of Temple in Bhagalpur.</b></marquee>
                </span>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <img src="bhgImg/2019-10-10 (1).jpg" style={{ width: '556px', height: '140px' }} className="img-responsive  left-block-shape1" alt=""/>
                <span className="heading2 itemcap">
                  <marquee scrollamount="4"><b>Const. Of Gazeboinstallation Of Fountain, Walkway, Street Lights, Horticulture,
                    Child Playing Equipments, Sculpture, Etc.</b></marquee>
                </span>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <img src="bhgImg/manjusa.1_0f609dd3-8edc-442a-9af8-b89376697612_1024x1024.jpg" style={{ width: '556px', height: '140px' }} className="img-responsive  left-block-shape1" alt=""/>
                <span className="heading2 itemcap">
                  <marquee scrollamount="4"><b> Reinforcement &amp; casting of drain going on. Site clearance is in progress.</b>
                  </marquee>
                </span>
              </div>
            </div>
          </div>
        </section>
        <section className="important1">
          <div className="container">
            <div className="row" style={{ paddingTop: '0px' }}>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media payment">
                  <div className="media-left media-middle book">
                    <img src="img/e-Payments.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">e-Payments</h4>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media payment">
                  <div className="media-left media-middle book">
                    <img src="img/icon-05.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">e-Grievances</h4>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media payment">
                  <div className="media-left media-middle book">
                    <img src="img/icon-09.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">e-Requests</h4>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="media payment">
                  <div className="media-left media-middle book">
                    <img src="img/icon-07.png" style={{ width: '60px' }} className="icon" alt="Post" />
                    <h4 className="media-heading">e-Booking</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="services" style={{ marginTop: '-30px' }}>
          <div className="container">
            <div className="row">
              <h2 className="section-title" style={{ color: '#3f3f3f' }}><b>QUICK LINKS</b></h2><b>
                <div className="col-md-4 col-sm-4 col-xs-12">
                  <a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/Finance-Money-Industry-Icon.png" style={{ width: '40px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">PROPERTY TAX</h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-md-4 col-sm-4 col-xs-12">
                  <a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/fire-department-icon-10.png" style={{ width: '40px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">Building Permission</h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-md-4 col-sm-4 col-xs-12">
                  <a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/icon-01-1.png" style={{ width: '40px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">Trade &amp; License</h4>
                      </div>
                    </div>
                  </a></div><a href="#">
                </a></b></div><b><a href="#">
                </a><div className="row"><a href="#">
                </a><div className="col-md-4 col-sm-4 col-xs-12"><a href="#">
                </a><a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/subscribe.png" style={{ width: '40px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">Water Connection </h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-md-4 col-sm-4 col-xs-12">
                  <a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/icon-01-2.png" style={{ width: '40px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">Sewerage Connection</h4>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-md-4 col-sm-4 col-xs-12">
                  <a href="#">
                    <div className="media Know_your_Office">
                      <div className="media-left media-middle">
                        <img src="img/img-05.png" style={{ width: '45px' }} className="icon" alt="Post" />
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">Birth &amp; Death</h4>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </b></div><b>
          </b></section><b>
          {/*desktop>*/}
          {/*mobile>*/}
          <section className="header-bottom home-area city_dashboard">
            <div className="home_content">
              <div className="container">
                <div className="row">
                  <h2 className="section-title" style={{ color: '#3f3f3f', fontSize: '20px' }}><b>CITY DASHBOARD</b></h2>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <div className="media business_development">
                      <div className="media-left media-middle book">
                        <h5 className="media-heading1"><span className="glyphicon glyphicon-circle-arrow-right" /> Street Lights </h5>
                        <img src="img/images-1-1.png" style={{ width: '90px' }} className="icon" alt="Post" />
                        <h1 style={{ fontWeight: 700 }}>123456</h1>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <div className="media business_development">
                      <div className="media-left media-middle book">
                        <h5 className="media-heading1"><span className="glyphicon glyphicon-circle-arrow-right" /> SCADA </h5>
                        <img src="img/icon-5.png" style={{ width: '90px' }} className="icon" alt="Post" />
                        <h1 style={{ fontWeight: 700 }}>123456</h1>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <div className="media business_development ">
                      <div className="media-left media-middle book">
                        <h5 className="media-heading1"><span className="glyphicon glyphicon-circle-arrow-right" /> DPMS </h5>
                        <img src="img/images-2.png" style={{ width: '90px' }} className="icon" alt="Post" />
                        <h1 style={{ fontWeight: 700 }}>123456</h1>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-sm-6 col-xs-12">
                    <div className="media business_development">
                      <div className="media-left media-middle book">
                        <h5 className="media-heading1"><span className="glyphicon glyphicon-circle-arrow-right" /> Revenue </h5>
                        <img src="img/IMG-04-1.png" style={{ width: '90px' }} className="icon" alt="Post" />
                        <h1 style={{ fontWeight: 700 }}>123456</h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="header-bottom home-area news">
            <div className="home_content">
              <div className="container">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="media">
                        <div className="media-left media-middle">
                          <h4 className="media-heading3"> <i className="fa fa-newspaper-o pull-left" style={{ background: 'transparent' }} aria-hidden="true" /> News &amp; update
                          </h4>
                          <ul id="nt-example2" className="w-100 nt-example-cls" style={{ height: '255px', overflow: 'hidden' }}>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> 145 MM dia pipe
                              laying completed for more than 11 km. Work is in full swing. </a>
                            </li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> PHED land transfer
                              for stp is pending. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Roof sheet work,
                              installation of HVAC &amp; acoustic boards. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Structural work up
                              to ground floor completed. Slab shuttering for next floor going on.
                              Brickwork started. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Construction of
                              above floors &amp; External development. </a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="media">
                        <div className="media-left media-middle">
                          <h4 className="media-heading3"><i className="fa fa-gavel pull-left" style={{ background: 'transparent' }} aria-hidden="true" /> Tenders &amp;
                            Notification </h4>
                          <ul id="nt-example2" className="w-100 nt-example-cls" style={{ height: '255px', overflow: 'hidden' }}>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Aerial survey work
                              is completed and contractor submitted the superimposed survey Map
                              Sbc test completed. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Design &amp; drawing
                              is in progress Site clearance work is in Progress Sonar survey work
                              completed. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Reinforcement &amp;
                              casting of drain going on. Site clearance is in progress. </a></li>
                            <li style={{ marginTop: '0px' }}><a href="#" target="_blank"> Redesign as per
                              bye-laws is completed dismantling of existing structure completed
                              excavation for foundation completed.</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="header-bottom home-area news">
            <div className="home_content">
              <div className="container">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="media">
                        <div className="media-left media-middle">
                          <h4 className="media-heading" style={{ textAlign: 'left', paddingBottom: '20px', color: '#3f3f3f' }}> <span className="fa fa-chevron-circle-right text-warning" aria-hidden="true" /> BHAGALPUR MUNICIPAL CORPORATION</h4>
                          <div className="left-block-shape" style={{ height: '515px', backgroundColor: '#eee', padding: '10px' }}>
                            <img src="bhgImg/shaanti-baba-temple-colgong.jpg" className="img-responsive  left-block-shape1" style={{ width: '100%' }} height="auto" alt="vmc" />
                            <p className="pt20 " style={{ width: '95%', fontSize: '12px', paddingTop: '10px', textAlign: 'justify' }}>
                              Hdd cable laying &amp; drone survey going on in full swing. Ofc network
                              (100km) Location identification completed. Itms including pa,vmd,
                              ecb,atcs (27 junctions) Detailed survey completed / cctv surveillance
                              (65 locations) - drawing and survey Report, preparation in progress,
                              priority 04 junction Atcs survey report submitted.
                              {/* <a href="#"   class="text-danger"  target="_blank"><span class="fa fa-arrow-circle-o-right" aria-hidden="true"></span>&nbsp;Read More...</a> */}
                            </p>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <img src="bhgImg/dolphin.jpg" className="img-responsive  left-block-shape1" style={{ width: '560px', height: '130px' }} alt="vmc" />
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <img src="bhgImg/manjusa.1_0f609dd3-8edc-442a-9af8-b89376697612_1024x1024.jpg" className="img-responsive  left-block-shape1" style={{ width: '560px', height: '130px' }} alt="vmc" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-sm-6 col-xs-12">
                      <div className="media">
                        <div className="media-left media-middle">
                          <h4 className="media-heading" style={{ textAlign: 'left', paddingBottom: '20px', color: '#3f3f3f' }}> <span className="fa fa-chevron-circle-right text-warning" aria-hidden="true" /> BHAGALPUR EVENTS &amp; PLACES</h4>
                          <div className="left-block-shape" style={{ height: '515px', backgroundColor: '#eee', padding: '10px' }}>
                            <div className="col-lg-9 col-md-9 col-sm-12">
                              <h4 style={{ textAlign: 'left' }}>Events </h4>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-3">
                              <a href="ShowallEventPhotos" className="btn btn-warning btn-xs know-more-setting" target="_blank"><span className="fa fa-arrow-circle-o-right" aria-hidden="true" />&nbsp;View More</a>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <img src="bhgImg/a3b27040-f066-4eb3-8047-88a87f17265c_Maharshi-Mehi-Ashram-Kuppaghat-Bhagalpur (1).jpg" className="img-responsive  left-block-shape1" style={{ width: '550px', height: '170px' }} alt="vmc" />
                                <span className="heading2 itemcap">
                                  <marquee scrollamount="4"><b>Re-development of city park
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Const. Of
                                    gazebo installation of fountain, walkway, street lights,
                                    horticulture, child playing equipments, sculpture,
                                    etc.</b></marquee>
                                </span>
                              </div>
                              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <img src="bhgImg/images-largejpg.jpg" className="img-responsive  left-block-shape1" style={{ width: '550px', height: '170px' }} alt="vmc" />
                                <span className="heading2 itemcap">
                                  <marquee scrollamount="4"><b>Re-development of jubba sahani park
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Const. Of
                                    gazebo Installation of fountain, walkway, Street lights,
                                    horticulture, child Playing equipments, sculpture,
                                    etc.</b></marquee>
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-9 col-md-9 col-sm-12">
                              <h4 style={{ textAlign: 'left' }}>Places </h4>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-3">
                              <a href="ShowallEventPhotos" className="btn btn-warning btn-xs know-more-setting" target="_blank"><span className="fa fa-arrow-circle-o-right" aria-hidden="true" />&nbsp;View More</a>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <img src="bhgImg/Manjusha-painting-Sumna-Devi-04-1.jpg" className="img-responsive  left-block-shape1" style={{ width: '550px', height: '170px' }} alt="vmc" />
                                <span className="heading2 itemcap">
                                  <marquee scrollamount="4"><b>Smart mini bus and e-rickshaw stops sheds
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Construction
                                    of twenty five(25) No of smart mini bus and E- rickshaw
                                    in muzaffarpur. It features latest models &amp; Amenities
                                    like ro water system For drinking purpose.</b></marquee>
                                </span>
                              </div>
                              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <img src="img/water-atm.jpg" className="img-responsive  left-block-shape1" style={{ width: '550px', height: '170px' }} alt="vmc" />
                                <span className="heading2 itemcap">
                                  <marquee scrollamount="4"><b>Installation of ro water atm at multiple location
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Installation
                                    of ro water atm coin operated vending machine at
                                    multiple location for safe &amp; clean drinking water to
                                    public.</b></marquee>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div></section>
        </b></main>
    </>
  )

}

export default Home;