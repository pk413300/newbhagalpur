import "./Aboutcity.css"

const Aboutcity = () => {
  return (
    <>
      <section className="main-body" style={{ padding: "30px 0px" }}>
        <div className="container headings-about">
          <h4 className="content-title-2" style={{ fontWeight: 670 }}>
            ABOUT CITY
          </h4>
          <h5 className="content-title-sub" style={{ fontWeight: 670 }}>
            Vision Bhagalpur
          </h5>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Citizens of Bhagalpur collectively envision a progressive city,
            dedicated towards promoting its unique identity, good governance,
            enhanced quality of life, sustainable utilization of its distinctive
            historical, cultural and ecological assets to attract tourism,
            investments, and business.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            Aspirations of citizens from which the vision emerged
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            The Vision for Bhagalpur has emerged from the SWOT analysis as well
            as the aspirations expressed by citizens. Bhagalpur aspires to be a
            city where municipal governance is in-sync with citizens’ needs and
            the citizenry is aware, and responsible, with a sense of ownership
            for the city. The citizens’ aspirations can be categorized into four
            broad heads from which specific goals have taken shape{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            1. Aspirations of Bhagalpur being a Smart City with an accountable,
            smart and efficient local Government striving for a livable city
            with smart services
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            2. Aspirations linked to due recognition of the city’s traditional
            industries and cultural and ecological and tourism assets leading to
            increased investments and tourist trade{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            3. Aspirations of improved intra-city and inter-city{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            4. Aspirations linked to presence of robust and well-functioning
            infrastructure that assures universal access and coverage
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            Goals
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Bhagalpur’s Vision comprises constituent development goals that have
            been shaped by the voice and aspirations of the citizens. Each of
            the goals listed below aims at making Bhagalpur a livable city which
            is socially, economically and ecologically sustainable.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            GOAL 1 : SUSHAASIT BHAGALPUR
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Smart and Quality Governance
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Bhagalpur Municipal Corporation recognizes the importance of
            addressing the needs of a growing population and an expanding city.
            It would strive to make Bhagalpur ‘a city for people’ where
            continuous engagement of local government with citizens is
            institutionalized in the decision making architecture. It would
            provide safe, accessible, and lively public spaces the city living
            experience and the river bank. Bhagalpur Municipal Corporation is
            committed to excellence in every sector, providing its staff
            opportunities of growth by learning and adopting state of the art
            technologies and methods to better their performance in every way.
            Its goal is to become financially self-sufficient and sustainable
            moving towards increased private investments and partnerships.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            GOAL 2 : SAMRUDH BHAGALPUR
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Promoting Tourism in Silk City of India
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            A historically and culturally significant city with ever-growing
            number of visitors, Bhagalpur Municipal Corporation is committed to
            preserve and promote its multi-faceted built heritage ranging from
            ancient Hindu rock-cut sculptures to Jain pilgrimage sites to
            archaeological remains of one of the biggest universities of
            Buddhist times to architecture from Mughal and British periods.
            Bhagalpur’s identity and image as a culturally and historically
            significant place will be promoted on regional, national and
            international level. The city will support regular religious
            festivals and celebrations and also design new city level events for
            residents and tourists. Bhagalpur will be the most visited place in
            Bihar in the years to come.
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Progressive Economy and Employment
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Bhagalpur is committed to promoting its unique Tussar silk industry
            and regaining its position in the world. The related sub-goals are
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (i) adequate space for retail within the city and swift regional and
            national movement of special woven products like silk saris, silk
            suit materials, Bhagalpuri blankets etc
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (ii) Silk industry’s expansion and regeneration
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (iii) Enhanced skills and adaptability of silk industry and craft
            persons
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (iv) Enhance production and provide comfortable circumstances to
            work with continuous power supply
          </p>
        </div>
        <div className="container headings-about">
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Conserving River Ganga and Promoting Eco-Tourism
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Located on the banks of Ganga, the city feels proud to keep the
            river clean and beautiful. The river will be more accessible to the
            public for people to admire the ecological wealth of their city.
            However, the river-side development and use would be carried out in
            an environmentally friendly way with strict rules so that the river
            and its eco-system are preserved.{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            The two associated sub-goals are{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (i) Bhagalpur will not dump solid waste in the Champa Nala, ponds of
            the city or the Ganga
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (ii) Bhagalpur will severely penalize dumping of untreated sewage in
            the Ganga.
          </p>
        </div>
        <div className="container headings-about">
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Land Monetization and Attracting Investment
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Bhagalpur Municipal Corporation will create business friendly
            environment by fast and efficient approval system. More land will be
            earmarked for commercial leasing; economic corridors will be
            developed, Land Monetization Plan will be prepared for capturing
            rising land values. Development projects will engage private players
            on PPP mode.{" "}
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            GOAL 3 : GATIMAAN BHAGALPUR{" "}
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► A well-connected city with enhanced public transport, BMC will
            develop multi-modal transport system and provide more intra and
            inter-city roadways and buses to ease people’s arrival to the city.
            It will improve vehicular movement and decongest roads. It will
            encourage NMT and pedestrian safety. Intelligent Transport System to
            be adopted to improve mobility especially of public transport and
            IPT. The city will have greens links connecting places of different
            landuse to enhance walkability.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            GOAL 4 : SUDRID BHAGALPUR
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► A well functioning, smart, and efficient city
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            Bhagalpur Municipal Corporation recognizes the current gaps in
            services especially Water supply, sanitation, SWM, and public
            transport. For a smoothly functioning city, new technology and ICT
            will be adopted. The power infrastructure will be robust and promote
            energy efficiency and use of renewable energy.
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            The sub-goals are –
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (i) Bhagalpur will not stop moving due to traffic jams;{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (ii) every resident and visitor to Bhagalpur will have access to
            clean drinking water;{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (iii) All streets will be clean and municipal waste will be treated
            and disposed scientifically;{" "}
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            (iv) Every household will be connected to a functional sanitation
            system
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            (v) city will have robust IT enabled infrastructure
          </p>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            {" "}
            (vi) All power cables to be underground.
          </p>
        </div>
        <div className="container headings-about">
          <h4 className="content-title-sub" style={{ fontWeight: 670 }}>
            GOAL 5 : SARVABHOUMIK VIKAS{" "}
          </h4>
          <p style={{ margin: "0px 0px 10px", color: "#3f3f3f" }}>
            ► Socially Inclusive Growth: Bhagalpur will be more sensitive
            towards social infrastructure by improving quality of Health,
            Education, Safety. All sections of society of Bhagalpur especially
            the old, children and differently abled will be taken care of in
            design and development of projects. The city will put in efforts to
            alleviate urban poverty and increase livelihood opportunities. The
            city will provide livable conditions for slum dwellers by
            upgradation of physical infrastructure. The informal sector will be
            regularised by providing basic infrastructure and facilities and
            formal vending zones.
          </p>
        </div>
      </section>
    </>
  );
};

export default Aboutcity;
