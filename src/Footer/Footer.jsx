const Footer = () => {
  return (
    <>
      <footer className="site-footer">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-1" />
            <div className="col-md-2 col-sm-6 col-xs-12 fbox">
              <h4>About Us</h4>
              <ul className>
                <li>
                  <a >
                    Project Deatils{" "}
                  </a>
                </li>
                <li>
                  <a >
                    Project Status{" "}
                  </a>
                </li>
                <li>
                  <a >
                    Ongoing Project
                  </a>
                </li>
                <li>
                  <a >
                    Commissioner
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-2 col-sm-6 col-xs-12 fbox">
              <h4>General Information</h4>
              <ul className>
                <li>
                  <a >
                    ULB Profile
                  </a>
                </li>
                <li>
                  <a >
                    Organization Chart
                  </a>
                </li>
                <li>
                  <a >
                    Ward Profile
                  </a>
                </li>
                <li>
                  <a >
                    Budget &amp; Finance
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-2 col-sm-6 col-xs-12 fbox">
              <h4>General Information</h4>
              <ul className>
                <li>
                  <a >
                    Act/Bye Laws
                  </a>
                </li>
                <li>
                  <a >
                    Important Links
                  </a>
                </li>
                <li>
                  <a >
                    Development
                  </a>
                </li>
                <li>
                  <a >
                    Welfare
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12 ">
              <div className="row">
                <div className="col-md-4 col-sm-12 col-xs-12  p-1">
                  {/* <h4></h4> */}
                  <img
                    src="./bhgImg/swachh-bharat-abhiyan.jpg"
                    style={{
                      width: "130px",
                      height: "80px",
                      marginBottom: "5px",
                    }}
                    alt=""
                  />
                </div>
                <div className="col-md-4 col-sm-12 col-xs-12 p-1">
                  {/* <h4></h4> */}
                  <img
                    src="./bhgImg/download (1).png"
                    style={{
                      width: "130px",
                      height: "80px",
                      marginBottom: "5px",
                    }}
                    alt=""
                  />
                </div>
                <div className="col-md-4 col-sm-12 col-xs-12 p-1">
                  {/* <h4></h4> */}
                  <img
                    src="./bhgImg/mygov_1446011863190667.jpg"
                    style={{
                      width: "130px",
                      height: "80px",
                      marginBottom: "5px",
                    }}
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="copyright">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <p className="pull-left">
                  © Copyright @ 2019. BSCL,
                  <br /> All Rights Reserved
                </p>
              </div>
             
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
