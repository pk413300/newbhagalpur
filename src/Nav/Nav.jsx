import "./Nav.css";
import { Link } from "react-router-dom";
import { useState, useContext } from "react";

import {
  FormattedMessage,
} from "react-intl";
import { Context } from "../Wrapper";

const Nav = (props) => {
  const context = useContext(Context);
  const [className, setClassName] = useState(0);

  return (
    <>
      <header className="site-header">
        <div className="top">
          <div className="container">
            <div className="row">
              <div className="col-sm-4 col-md-4">
                <i className="fa fa-sun-o" aria-hidden="true"></i> &nbsp;&nbsp;{" "}
                <i className="fa fa-adjust" aria-hidden="true"></i> &nbsp;&nbsp;
                A- &nbsp;&nbsp; A &nbsp;&nbsp; A+
              </div>
              <div className="col-sm-4 col-md-4"  >


                <div style={{ display: "flex", width: "300px" }}>
                  <div style={{ marginRight: "4px", width: "110px" }} > <p  >
                    {" "}
                    <FormattedMessage
                      id="language.choose"
                      defaultMessage="Choose Language"
                    />
                  </p></div>

                  <select
                    style={{ backgroundColor: "#016879", width: "80px" }}
                    value={context.locale}
                    onChange={context.selectLanguage}
                  >
                    <option className="lang_option" value="en">English</option>

                    <option className="lang_option" value="hi">हिंदी</option>
                  </select>
                </div>


              </div>
              <div className="col-sm-4  col-md-4">
                <ul className="list-inline pull-right">
                  <li>
                    <a
                      href="#"
                      style={{ backgroundColor: "#f47f20", padding: "3px" }}
                    >
                      <FormattedMessage
                        id="app.login"
                        defaultMessage="LOGIN"

                      />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <nav className="navbar navbar-default" >
          <div
            className="container"
            style={{
              backgroundImage: "url(bhgImg/download.png)",
              height: "150px",
              backgroundPosition: "right",
              backgroundRepeat: "no-repeat",
              backgroundSize: "contain",
              display: "flex",
              alignItems: "center",
              zIndex: "23"

            }}
          >
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-navbar-collapse"
            >
              <span className="sr-only">Toggle Navigation</span>
              <i className="fa fa-bars"></i>
            </button>
            <Link to="/"> <a className="navbar-brand">
              <img
                src="bhgImg/1 (1).JPG"
                style={{ width: "120px" }}
                alt="Post"
              ></img>
            </a>
            </Link>
            <h3
              style={{
                color: "#016879",
                fontWeight: 900,
                fontFamily:
                  '"Lucida Sans", "Lucida Sans Regular", "Lucida Grande", "Lucida Sans Unicode", Geneva, Verdana, sans-serif',
              }}
            >
              BHAGALPUR <br /> SMART CITY LIMITED
            </h3>
          </div>
        </nav>

        <nav
          className="navbar navbar-default"
          style={{ backgroundColor: "#fff" }}
        >
          <div className="container">
            <div className="collapse navbar-collapse" id="bs-navbar-collapse">
              <ul className="nav navbar-nav main-navbar-nav">
                <li
                  className={
                    className == 0 ? "active active_link" : "active_link"
                  }

                  onClick={() => {
                    setClassName(0);
                  }}
                >
                  <Link to="/">
                    <p style={{ fontWeight: "600", color: "#4a8db4" }}>
                      <FormattedMessage
                        id="app.home"
                        defaultMessage="Home"

                      />
                    </p>
                  </Link>
                </li>

                <li
                  className={
                    className == 1
                      ? "active nav-item dropdown"
                      : "nav-item dropdown"
                  }
                >
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    style={{ fontWeight: "600", color: "#4a8db4" }}
                  >
                    <FormattedMessage
                      id="app.about.us"
                      defaultMessage="About Us"

                    />
                    <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    aria-labelledby="navbarDropdown"

                    style={{ backgroundColor: "#016879" }}
                  >
                    <li

                    // onClick={() => {
                    //   setClassName(1);
                    // }}
                    className="drop"

                    >
                      <Link to="/aboutcity">
                        <div  >
                          <FormattedMessage
                            id="app.about.city"
                            defaultMessage="About City"

                          />
                        </div>
                      </Link>
                    </li>
                    <li
                      className="about_us"
                    // onClick={() => {
                    //   setClassName(1);
                    // }}
                    >
                      <Link to="/cityprofile">
                        <div >
                          <FormattedMessage
                            id="app.city.profile"
                            defaultMessage="City Profile"

                          />
                        </div>
                      </Link>
                    </li>
                    <li
                      className="about_us"
                      onClick={() => {
                        setClassName(1);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>
                          <FormattedMessage
                            id="board.of.director"
                            defaultMessage="Board Of Director"

                          />
                        
                        </div>
                      </Link>
                    </li>
                    <li
                      className="about_us"
                      onClick={() => {
                        setClassName(1);
                      }}
                    >
                      <Link to="/team">
                        <div>
                          <FormattedMessage
                            id="team.of"
                            defaultMessage="Team of"

                          />
                          &nbsp;BSCL
                        </div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li>
                  <a style={{ fontWeight: "600", color: "#4a8db4" }}>
                    <FormattedMessage
                      id="app.department"
                      defaultMessage="Departments"

                    />
                    <b className="caret"></b>
                  </a>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    style={{ fontWeight: "600", color: "#4a8db4" }}
                  >
                    <FormattedMessage
                      id="app.e.service"
                      defaultMessage="e-Services"

                    />{" "}
                    <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundcColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div>  <FormattedMessage
                          id="app.property.tax"
                          defaultMessage="Property Tax"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div> <FormattedMessage
                          id="app.building.permission"
                          defaultMessage="Building Permission"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.trade.license"
                          defaultMessage="Trade and License"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.grievence"
                          defaultMessage="Grievence"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.birth.death"
                          defaultMessage="Birth &
                          Death Registration"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.water.connection"
                          defaultMessage="Water
                          Connection"
                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.sewage.connection"
                          defaultMessage="Sewage
                          Connection"
                        /></div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    style={{ fontWeight: "600", color: "#4a8db4" }}
                  >
                    <FormattedMessage
                      id="app.e.payment"
                      defaultMessage="e-Payments"

                    />{" "}
                    <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f " }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.pay.property.tax"
                          defaultMessage="Pay Property tax "

                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.water.charge"
                          defaultMessage="Water
                          Charges"

                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div> <FormattedMessage
                          id="app.trade.license.fee"
                          defaultMessage="Trade License Fee"

                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div> <FormattedMessage
                          id="app.sewerage.charge"
                          defaultMessage="Sewerage
                          Charges"

                        /></div>
                      </Link>
                    </li>
                  </ul>
                </li>
                <li>
                  <a style={{ fontWeight: "600", color: "#4a8db4" }}>
                    <FormattedMessage
                      id="app.dashboard"
                      defaultMessage="Dashboard"

                    />{" "}
                    <b className="caret"></b>
                  </a>
                </li>
                <li
                  className={className == 2 ? "active" : ""}
                  onClick={() => {
                    setClassName(2);
                  }}
                >
                  <Link to="/contact">
                    <div style={{ fontWeight: "600", color: "#4a8db4" }}>
                      {" "}
                      <FormattedMessage
                        id="app.contact"
                        defaultMessage="Contact Us"

                      />{" "}
                    </div>
                  </Link>
                </li>
                <li>
                  <a style={{ fontWeight: "600", color: "#4a8db4" }}>
                    <FormattedMessage
                      id="app.career"
                      defaultMessage="Careers"

                    />
                    <b className="caret"></b>
                  </a>
                </li>

                <li
                  className="nav-item dropdown"
                  onClick={() => {
                    setClassName(3);
                  }}
                >
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    style={{ fontWeight: "600", color: "#4a8db4" }}
                  >
                    <FormattedMessage
                      id="app.information"
                      defaultMessage="Information"

                    />{" "}
                    <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.event"
                          defaultMessage="Event"

                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div> <FormattedMessage
                          id="app.geo.circular"
                          defaultMessage="Geo & Circular"

                        /></div>
                      </Link>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    style={{ fontWeight: "600", color: "#4a8db4" }}
                  >
                    <FormattedMessage
                      id="app.tender"
                      defaultMessage="Tenders"
                      values={{
                        fileName: "src/App.js",
                        code: (word) => <strong>{word}</strong>,
                      }}
                    />{" "}
                    <b className="caret"></b>
                  </a>
                  <ul
                    className="dropdown-menu ul_cont"
                    style={{ backgroundColor: "#2eae9f" }}
                    aria-labelledby="navbarDropdown"
                  >
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.open.tender"
                          defaultMessage="Open Tender"

                        /></div>
                      </Link>
                    </li>
                    <li
                      className="drop"
                      onClick={() => {
                        setClassName(3);
                      }}
                    >
                      <Link to="/underconstruction">
                        <div><FormattedMessage
                          id="app.close.tender"
                          defaultMessage="Closed Tender "

                        /></div>
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    </>
  );
};

export default Nav;
