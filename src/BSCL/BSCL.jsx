import "./BSCL.css"

const BSCL = () => {
  return (<>
    <section className="main-body" style={{ padding: '30px 0px' }}>
      <div className="container headings-about">
        <h4 className="content-title-2" style={{ fontWeight: 670 }}>TEAM OF BSCL</h4>
        <div>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Anand Kishor,IAS</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Chairman</h6>
                    <p className="card-text" style={{ color: '#3f3f3f' }}>
                    </p><p style={{ color: '#3f3f3f' }} />
                    <p />
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Subrat Kumar Sen, IAS </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#532323' }}>Nominee Director</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Mithilesh Mishra, IAS</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Nominee Director</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Satish Kumar Singh, IAS </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Nominee Director</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Sanjeet Kumar Bhagat </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Nominee Director</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Prafull Chandra Yadav
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>MD-cum-Chief Executive Officer </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>ceo@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Sandip Kumar </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Chief General Manager</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>cgm@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>CA Sushil Kumar</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Chief Finance Officer
                    </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>cfo@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>CS Neha Sh
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Company Secretary</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>cs@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri T R Prashant </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Senior Manager (Technical) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>sr.manager@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Mukul Kumar Singh </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Senior Manager (Technical)
                    </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>sr.manager2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Pankaj Kumar </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Technical) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      manager.technical@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row ">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Avinash Kumar Yadav</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Technical) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>manager.technical2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Manish Kumar Singh
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Technical) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      manager.technical3@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Tapan Kumar Goswami</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Finance &amp; Procurement) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>manager.fnp@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Tushar Kumar</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Monitoring &amp; Evaluation) </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>manager.mne@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row ">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Syed Shaheer Azaz </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Manager (Implementation &amp; Control)</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>manager.inc@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Nigam Mishra
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Accountant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      accountant1@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Jitendra Kumar</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Accountant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>accountant2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Kunal Mishra</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>oea1@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row ">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Smt. Babita Kumari</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>oea2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Ms. Shweta Kumari
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      oea3@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Mohammad Shafhad</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>oea5@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Rakesh Kumar Bishwas </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>oea6@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row ">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Vivek Roy </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Office Executive-cum-Assistant </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>oea7@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Ms. Moni Kumar
                    </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Stenographer </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      steno2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Nishant Kumar Sahni </h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Computer Operator </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>co1@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Abhimanyu Kumar</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Computer Operator</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>co2@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row row_container_last">
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Ms. Taskeen Fatma</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Computer Operator</h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>co3@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}> Amit Kumar Bishwakarm
                    </h5>
                    {/* <h5 class="card-text" style="font-weight: 600;color: #3f3f3f; padding: 0;">Bishwakarma</h5> */}
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Computer Operator </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>
                      co4@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="card" style={{ width: '90%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                  <img src="../img/WhatsApp_Image_2022-04-30_at_1.35.10_PM-removebg-preview.png" className="card-img-top image_container" alt="..." />
                  <div className="card-body card_text_align">
                    <h5 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Shri Jaykant Yadav</h5>
                    <h6 className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>Computer Operator </h6>
                    <p className="card-text" style={{ fontWeight: 600, color: '#3f3f3f' }}>co5@smartcitybhagalpur.org
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
          </div>
          <div>

          </div>
        </div>
      </div></section>

  </>)
}

export default BSCL;