import "./UnderDev.css"

const UnderDev = () => {


    return (
        <>
            <section className="main-body" style={{ padding: '30px 0px' }}>
                <div className="container headings-about under_con_css">  <h4 id="content-title-2">Under Development</h4><img style={{ maxWidth: '100%', width: '70%', height: 'auto', minWidth: '280px' }} src="../img/Under construction-pana.svg"alt="" /></div>
            </section>
        </>
    )
}

export default UnderDev;